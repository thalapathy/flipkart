package com.org.jspiders.Flipkart.Entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.org.jspiders.Flipkart.constant.Appconstant;



@Entity
@Table(name=Appconstant.FLIPKART_INFO)

public class Flipkart implements Serializable {

	
	@Id
	@Column(name="id")
	private Long id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="Origin_Contry")
	private String Origin_Contry;
	
	@Column(name="brand_value")
	private Double brand_value;
	
	@Column(name="establised_year")
	private Date establised_year;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOrigin_Contry() {
		return Origin_Contry;
	}

	public void setOrigin_Contry(String origin_Contry) {
		Origin_Contry = origin_Contry;
	}

	public Double getBrand_value() {
		return brand_value;
	}

	public void setBrand_value(Double brand_value) {
		this.brand_value = brand_value;
	}

	public Date getEstablised_year() {
		return establised_year;
	}

	public void setEstablised_year(Date establised_year) {
		this.establised_year = establised_year;
	}
	
	
	
}
